

# 1. quick-find
# 2. quick-union
# 3. weighted QU
# 4. QU + path compression
# 5. weighted QU + path compression

import time
import random
#import matplotlib.pyplot as plt

class UF(object):
    """Union Find class

    """

    def __init__(self):
        self.id = []
        self.size=[]

    def qf_init(self, N):
        """initialize the data structure

        """
        self.a = N
        for x in range(N):
            self.id.append(x)
            self.size = [1]* N
    def qf_union(self, p, q):
        """Union operation for Quick-Find Algorithm.

        connect p and q. You need to
        change all entries with id[p] to id[q]
        (linear number of array accesses)

        """
        pid=self.id[p]
        qid=self.id[q]
        for x in range(self.a):
            if self.id[p]== pid:
                self.id[q]== qid
        return 1

    def qf_connected(self, p, q):
        """Find operation for Quick-Find Algorithm.
        simply test whether p and q are connected

        """
        if self.id[p] == self.id[q]:
            return True
        else:
            return False

        def root(self, p):
            while p != self.id[p]:
                p = self.id[p]
            return p
    # Quick Union Algorithm
    def qu_union(self, p, q):
        """Union operation for Quick-Union Algorithm.
         connect p and q.

         """
        pid = self.find(p)
        qid = self.find(q)
        self.id [pid]  = qid
        return 1


    def qu_connected(self, p, q):
        """Find operation for Quick-Union Algorithm.
         test whether p and q are connected

         """
        if self.find(q) == self.find(p):
            return True


    def wqu_union(self, p, q):
        root_q = self.qu_find(q)
        if not self.qu_connected(p, q):

            if self.sz[q] > self.sz[p]:
                self.id[root_p] = root_q
                self.sz[q] += self.sz[p]
            else:
                self.id[root_q] = root_q
                self.sz[q] += self.sz[q]
                self.count -= 1

    def wqu_connected(self, p, q):
        """Find operation for Weighted Quick-Union Algorithm.
         test whether p and q are connected

         """
        if self.qu_find(p) == self.qu_find(q):
            return True



    def pqu_union(self, p, q):
        """Union operation for path compressed Quick-Union Algorithm.
         connect p and q.

         """
        root_p = self.path_find(p)
        root_q = self.path_find(q)
        if self.pqu_connected(p, q):
            return 1
        else:
            self.id[root_p] = root_q
            self.count -= 1


    def pqu_connected(self, p, q):
        """Find operation for path compressed Quick-Union Algorithm.
         test whether p and q are connected

         """
        if self.path_find(p) == self.path_find(q):
           return True

    def wpqu_union(self, p, q):
        """Union operation for Weighted path compressed Quick-Union Algorithm.
         connect p and q.

         """
        root_p = self.path_find(p)
        root_q = self.path_find(q)
        if self.sz[root_p] < self.sz[root_q]:
            self.id[root_p] = root_q
            self.sz[root_q] += self.sz[root_p]
        else:
            self.id[root_q] = root_p
            self.sz[root_p] += self.sz[root_q]

        return 1

    def wpqu_connected(self, p, q):
        """Find operation for Weighted path compressed Quick-Union Algorithm.
         test whether p and q are connected

         """
        if self.path_find(p) == self.path_find(q):
         return True

if __name__ == "__main__":

    # iteration
    set_szs = [1000]
    timing = []

    # gives the timing for union operation only, you might want to do this for all functions you wrote.
    for set_sz in set_szs:
        # initialize network nodes
        inodes = UF()
        inodes.qf_init(set_sz)

        t0 = time.time()

        for idx in range(set_sz - 1):
            rp = random.randint(0, set_sz - 1)
            rq = random.randint(0, set_sz - 1)

            inodes.qf_union(rp, rq)

        t1 = time.time()

        total_time = t1 - t0

        timing.append(total_time)

        print((total_time))

    # this plots things in log scale (pls google it), you need to add matplotlib to your virtualenv first!


    # plt.plot(set_szs, timing)
     #plt.xscale('log')
     #plt.yscale('log')
     #plt.title('log')
     #plt.ylabel('some numbers')
     #plt.show()