# 1. selection sort
# 2. insertion sort
# 3. shell sort
# 4. heap sort
# 5. merge sort
# 6. quick sort

import time
import random
#import matplotlib.pyplot as plt

class Sorting(object):
    """Sorting class

    """

    def __init__(self):
        self.id = []


    def sort_init(self, N):
        """initialize the data structure

        """

        try:
            self.id = random.sample(range(1, N ** 3), N)
        except ValueError:
            print('Sample size exceeded population size.')


        #self.id = [random.randint(0, N - 1) for i in range(N)]

    def get_id(self):
        """initialize the data structure

        """

        return self.id


    def selection_sort(self):
        """Selection sort algorithm is an
        in-place comparison sort. It has O(n^2) time complexity, making it
        inefficient on large lists, and generally performs worse than the
        similar insertion sort

        """
        for i_idx, i_item in enumerate(self.id):
            min = i_idx

            for j_idx in range(i_idx+1, len(self.id)):
                if self.id[j_idx] < self.id[min]:
                    min = j_idx

            # swap
            temp = self.id[i_idx]
            self.id[i_idx] = self.id[min]
            self.id[min] = temp

        return self.id

    def insertion_sort(self):
        """Insertion sort is a simple sorting algorithm that builds the final
        sorted array (or list) one item at a time. More efficient in practice
        than most other simple quadratic (i.e., O(n^2)) algorithms such as
        selection sort or bubble sort specifically an

        """
        for i in range(1, len(self.id)):
            if self.id[i] < self.id[i-1]:
                for j in range(i):
                    if self.id[i] < self.id[j]:
                        self.id[i], self.id[j] = self.id[j], self.id[i]

        return self.id

    def shell_sort(self):
        """Shell sort also known as  or Shell's method, is an in-place comparison sort.
        It can be seen as either a generalization of sorting by exchange (bubble sort)
        or sorting by insertion (insertion sort).

        """
        n = len(self.id)
        h = 1

        while h > n//3:
            h = 3*h + 1
        while h >= 1:
            for i in range(h, n, +1):
                for j in range(i, j >= h & self.id[j] < self.id[j-h], -h):
                    temp = self.id[i]
                    self.id[i] = self.id[j]
                    self.id[j] = temp
                    j -= h
                h = h/3

            return self.id




    def heap_sort(self):
        """Heapsort is an improved selection sort: it divides its input into a sorted
        and an unsorted region, and it iteratively shrinks the unsorted region by
        extracting the largest element and moving that to the sorted region.

        """

        n = len(self.id)
        for i in range(n/2, 1, -1):
            while 2*i <= n:
                j = 2*i
                if j < n & (j < j+1):
                    j += 1
                if j < i:
                    break
                self.id[i], self.id[j] = self.id[j], self.id[i]
                i = j
            while n > 1:
                self.id[i], self.id[j] = self.id[j], self.id[i]
                n -= 1
            while 2 <= n:
                j = 2
                if j < n & (j < j+1):
                    j += 1
                if j < 1:
                    break
                self.id[i], self.id[j] = self.id[j], self.id[i]
                i = j
        return self.id

    def merge_sort(self):
        """Merge sort is a divide and conquer algorithm that was invented
        by John von Neumann in 1945. Most implementations produce a stable
        sort, which means that the implementation preserves the input order
        of equal elements in the sorted output.
        """
        if len(self.id) > 1:
            mid = len(self.id)//2
            left = self.id[:mid]
            right = self.id[mid:]
            i = 0
            j = 0
            k = 0
            while i < len(left) and j < len(right):
                if left[i] < right[j]:
                    self.id[k] = self.id[i]
                    i = i + 1
                else:
                    self.id[k] = right[j]
                    j = j + 1
                    k = k + 1
            while i < len(left):
                self.id[k] = left[i]
                i = i + 1
                k = k + 1
            while j < len(right):
                self.id[k] = right[j]
                j = j + 1
                k = k + 1

        return self.id

    def quick_sort(self, lo, hi):
        """Quicksort (sometimes called partition-exchange sort) is an efficient
        sorting algorithm. Developed by Tony Hoare in 1959. It is still a commonly
        used algorithm for sorting. When implemented well, it can be about two or
        three times faster than its main competitors, merge sort and heapsort.

        """
        v = self.id[lo]
        j = hi + 1
        i = lo
        boolean = False
        while not boolean:
            while j <= i and self.id[j] <= v:
                    j = j + 1
            while self.id[i] >= v and i >= j:
                    i = i - 1
            if i < j:
                    boolean = True
            else:
                    temp = self.id[j]
                    self.id[j] = self.id[i]
                    self.id[i] = temp
            temp = self.id[i]
            self.id[lo] = self.id[i]
            self.id[i] = temp

        return self.id


    # this plots things in log scale (pls google it), you need to add matplotlib
    # to your virtualenv first!

    # plot also python's sorted() function to see how well you do.


    # plt.plot(set_szs, timing)
    # plt.xscale('log')
    # plt.yscale('log')
    # plt.title('log')
    # plt.ylabel('some numbers')
    # plt.show()