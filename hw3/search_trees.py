
import time
import random


class Array_Search:
    def __init__(self, array):
        self.array = array

    def init_array_search(self, val_array):
        self.array = Array_Search(val_array)

    def squential_search(self, key):

        idx = 0
        for num in self.array:
            if num == key:
                return idx
            idx = idx+1
        return False

    def bsearch(self, val):
        low, high = 0, len(self) - 1
        while low <= high:
            mid = (low + high) / 2
            if self[mid] < val:
                low = mid + 1
            elif val < self[mid]:
                high = mid - 1
            else:
                return mid


class BST_Node:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None



class BST:
    def __init__(self):
        self.root = None
        self.size = 0

    def init_bst(self, val):
        self.root = BST_Node(val)

    def insert(self, val):
        if (self.root is None):
            self.init_bst(val)
        else:
            self.insertNode(self.root, val)

    def insertNode(self, current, val):
        if val <= current.val:
            if current.left:
                self.insertNode(current.left, val)
            else:
                current.left = BST_Node(val)
        elif val > current.val:
            if current.right:
                self.insertNode(current.right, val)
        else:
            current.right = BST_Node(val)

        return False

    def bsearch(self, val):

        return self.searchNode(self.root, val)

    def searchNode(self, current, val):
        if current is None:
            return False
        elif val == current.val:
            return True
        elif val < current.val:
            return self.searchNode(current.left, val)
        else:
            return self.searchNode(current.right, val)


    def delete(self, val):
        if self.size > 1:
            node = self.searchNode(val, self.root)
            if node:
                self.delete(node)
                self.size = self.size - 1
            elif self.size == 1 and self.root.val == val:
                self.root = None
                self.size = self.size - 1

            return False



class RBBST_Node:
    def __init__(self, val, color):
        self.val = val
        self.left = None
        self.right = None
        self.color = color
        self.size = 1


RED = True
BLACK = False


class RBBST:
    def __init__(self):
        self.root = None

    def init_rbbst(self, val, color):
        self.root = RBBST_Node(val, color)

    def is_red(self, current):
        if not current:
            return False
        return True


    def rotate_left(self, current):
        self.is_red(current.right)
        new_left_size = current.size - current.right.size + self.size(current.right.left)
        new_right_size = current.size
        right_node = current.right
        current.right = right_node.left
        right_node.left = current
        right_node.color = current.color
        current.color = RBBST_Node.RED
        current.size = new_left_size
        right_node.size = new_right_size
        return right_node

    def rotate_right(self, current):
        self.is_red(current.left)
        new_right_size = current.size - current.left.size + self.size(current.left.right)
        new_left_size = current.size
        left_node = current.left
        current.left = left_node.right
        left_node.right = current
        left_node.color = current.color
        current.color = RBBST_Node.RED
        current.size = new_right_size
        left_node.size = new_left_size
        return left_node

    def flip_colors(self, current):
        assert not self.is_red(current)
        self.is_red(current.left)
        self.is_red(current.right)
        current.color = RBBST_Node.RED
        current.left.color = RBBST_Node.BLACK
        current.right.color = RBBST_Node.BLACK

        return False

    def insert(self, val):
        if self.root is None:
            self.init_rbbst(val, RED)
        else:
            self.insertNode(self.root, val)


    def insertNode(self, current, val):
        if val < current.val:
            current.left = self.insertNode(current.left, val)
            current.size += 1
        elif val > current.val:
            current.right = self.insertNode(current.right, val)
            current.size += 1
        else:
            current.val = val
        if self.is_red(current.right) and not self.is_red(current.left):
            current = self.rotate_left(current)
        if self.is_red(current.left) and self.is_red(current.left.left):
            current = self.rotate_right(current)
        if self.is_red(current.right) and self.is_red(current.left):
            self.flip_colors(current)
        return current


    def bsearch(self, val):

        return self.searchNode(self.root, val)

    def searchNode(self, current, val):

        while current:
            if val == current.val:
                return current
            if val < current.val:
                current = current.left
            else:
                current = current.right

        return current

if __name__ == "__main__":


    set_sz = 10
    tut = BST()

    vals = random.sample(range(1, 100), set_sz)

    for idx in range(set_sz - 1):

        tut.insert(vals[idx])

    print (tut.bsearch(vals[1]))
    print(tut.bsearch(11))

    tut_rb = RBBST()

    for idx in range(set_sz - 1):

        tut_rb.insert(vals[idx])

    print (tut.bsearch(vals[1]))
    print(tut.bsearch(11))