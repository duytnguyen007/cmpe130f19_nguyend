from collections import defaultdict

class Graph:
	def __init__(self):
		self.graph = defaultdict(list)

	def addEdge(self,u,v):
        self.graph[u].append(v)

    def dfsutil(self, v, visited):
    # Mark the current node as visited and print it
        visited[v] = True
        for i in self.graph[v]:
            if visited[i] == False:
                self.dfsutil(i, visited)


    def dfs(self, v):
        visited = [False] * (len(self.graph))
        self.dfsutil(v, visited)

    def bfs(self, v):
        visited = [False] * (len(self.graph))
        queue = []
        queue.append(v)
        visited[v] = True

        while queue:
            s = queue.pop(0)
            print(s, end=" ")
            for i in self.graph[s]:
                if visited[i] == False:
                    queue.append(i)
                    visited[i] = True
                    return 1
