1class Digraph:
    """This class implements a directed graph with nodes represented by integers. """

    def __init__(self):
        """Initializes this digraph."""
        self.nodes = set()
        self.edges = 0

    def add_node(self, node):
        """adds vertices to your graph"""
        self.nodes.add(node)
        self._adj[node] = dict()



    def add_edge(self, last, first):
        """creates edges between two given vertices in your graph"""
        if last not in self.nodes:
            raise ValueError("last is not a node")
        if first not in self.nodes:
            raise ValueError("first is not a node")

        self.edges.append((last, first))
        self._adj[last].append(first)

        return 1

    def has_edge(self, first, last, key = None):
        """checks if a connection exists between two given nodes in your graph"""
        try:
            if key is None:
                return last in self._adj[first]
            else:
                return key in self._adj[first][last]
        except KeyError:
            return False

        return 1

    def remove_edge(self, last, first):
        """removes edges between two given vertices in your graph"""
        try:
            self.edges.remove((last, first))
        except KeyError:
            raise ValueError("graph does not contain edge: ({0}, {1})".format(last, first))
        self._adj[last].remove(first)


    def remove_node(self, node):
        """removes vertices from your graph"""
        try:
            self.vertices.remove(node)
        except KeyError:
            raise ValueError(
                "graph does not contain vertex: {0}".format(node)
            )

            # delete _adjacencies
        del self._adj[node]
        for adj in self._adj.values():
            adj[:] = [v for v in adj if v != node]

        # delete edges
        self.edges = [
            e for e in self.edges if node not in (e[0], e[1])
        ]
    def contains(self, node):
        """checks if your graph contains a given value"""
        return self.nodes.issuperset(node)


